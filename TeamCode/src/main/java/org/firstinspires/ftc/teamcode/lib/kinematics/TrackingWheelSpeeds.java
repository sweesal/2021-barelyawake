package org.firstinspires.ftc.teamcode.lib.kinematics;

public class TrackingWheelSpeeds {

    public double leftMetersPerSecond;
    public double rightMetersPerSecond;
    public double horizontalMetersPerSecond;

    public TrackingWheelSpeeds(
            double rightMetersPerSecond,
            double leftMetersPerSecond,
            double horizontalMetersPerSecond) {
        this.rightMetersPerSecond = rightMetersPerSecond;
        this.leftMetersPerSecond = leftMetersPerSecond;
        this.horizontalMetersPerSecond = horizontalMetersPerSecond;
    }

}
