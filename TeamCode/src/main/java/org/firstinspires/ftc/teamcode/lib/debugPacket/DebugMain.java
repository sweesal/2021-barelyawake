package org.firstinspires.ftc.teamcode.lib.debugPacket;


import org.firstinspires.ftc.teamcode.lib.debugPacket.util.ComputerDebugging;
import org.firstinspires.ftc.teamcode.lib.debugPacket.util.FloatPoint;
import org.firstinspires.ftc.teamcode.lib.debugPacket.util.Robot;


public class DebugMain {


    public static void main(String[] args) {
        new DebugMain().run();
    }

    public void init() {

    }

    public void loop() {
        PathGenerator.sendToDebugging();
    }

    /**
     * The program runs here
     */
    public void run(){
        //this is a test of the coding
        ComputerDebugging computerDebugging = new ComputerDebugging();
        Robot robot = new Robot();
        init();

        ComputerDebugging.clearLogPoints();


        long startTime = System.currentTimeMillis();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        while(true){

           loop();

            try {
                Thread.sleep(30);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            robot.update();
            ComputerDebugging.sendRobotLocation(robot);
            ComputerDebugging.sendLogPoint(new FloatPoint(Robot.worldXPosition, Robot.worldYPosition));
            ComputerDebugging.markEndOfUpdate();
        }
    }




}
