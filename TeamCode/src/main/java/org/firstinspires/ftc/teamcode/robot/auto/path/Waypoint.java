package org.firstinspires.ftc.teamcode.robot.auto.path;

public class Waypoint extends Point {
    public double velocity = 0;

    public Waypoint(double x, double y, double velocity) {
        super(x, y);
        this.velocity = velocity;
    }

    public Waypoint(double x, double y) {
        this(x, y, 0);
    }

    public String toString() {
        return x + ", " + y + ", " + velocity;
    }
}
