package org.firstinspires.ftc.teamcode.lib.debugPacket.util;

public class FloatPoint {
    public double x;
    public double y;
    public FloatPoint(double x, double y){
        this.x = x;
        this.y = y;
    }
}
