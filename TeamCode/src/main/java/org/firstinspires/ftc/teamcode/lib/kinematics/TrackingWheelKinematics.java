package org.firstinspires.ftc.teamcode.lib.kinematics;

import org.ejml.simple.SimpleMatrix;

public class TrackingWheelKinematics {
    private SimpleMatrix fwdKinematics;
    private SimpleMatrix invKinematics;
    private SimpleMatrix trackingWheelMatrix = new SimpleMatrix(3, 1); // 3*1 matrix
    private SimpleMatrix chassisSpeedMatrix = new SimpleMatrix(3, 1); // 3*1 matrix

    public static void main (String[] args) {
        TrackingWheelKinematics kinematics = new TrackingWheelKinematics(30, 2);
        SimpleMatrix result = kinematics.testInverse();
        for (int i = 0; i < 3; i ++) {
            System.out.print(result.get(i, 0) + "\t");
            System.out.print(result.get(i, 1) + "\t");
            System.out.print(result.get(i, 2) + "\t\n");
        }
    }

    public SimpleMatrix testInverse () {
        return fwdKinematics.mult(invKinematics);
    }

    public TrackingWheelKinematics() {}

    public TrackingWheelKinematics(double xAxisTrackWidth, double yAxisOffset) {
        invKinematics = new SimpleMatrix(3, 3);
        setInvKinematics(xAxisTrackWidth/2, yAxisOffset); // make values half
        fwdKinematics = new SimpleMatrix(3, 3);
        //fwdKinematics = invKinematics.pseudoInverse();
        setFwdKinematics(xAxisTrackWidth/2, yAxisOffset);
    }

    private void setFwdKinematics (double xOffset, double yOffset) {
        fwdKinematics.setRow(0, 0, 0.5, 0, 0.5);
        fwdKinematics.setRow(1, 0,  yOffset / (2 * xOffset), 1, - yOffset / (2 * xOffset));
        fwdKinematics.setRow(
                2, 0, - 1 / (2 * xOffset), 0, 1 / (2 * xOffset));
    }

    private void setInvKinematics (double xOffset, double yOffset) {
        invKinematics.setRow(0, 0, 1, 0, -xOffset);
        invKinematics.setRow(1, 0, 0, 1, yOffset);
        invKinematics.setRow(2, 0, 1, 0, xOffset);
    }

    public ChassisSpeeds toChassisSpeed (TrackingWheelSpeeds trackingWheelSpeeds){
        trackingWheelMatrix.setColumn(0, 0,
                trackingWheelSpeeds.leftMetersPerSecond, trackingWheelSpeeds.horizontalMetersPerSecond, trackingWheelSpeeds.rightMetersPerSecond); // Anticlockwise.
        chassisSpeedMatrix = fwdKinematics.mult(trackingWheelMatrix);
        return new ChassisSpeeds(
                chassisSpeedMatrix.get(0, 0),
                chassisSpeedMatrix.get(1, 0),
                chassisSpeedMatrix.get(2, 0)
        );
    }

}
