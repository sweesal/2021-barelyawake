package org.firstinspires.ftc.teamcode;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.drive.SampleMecanumDrive;
import org.firstinspires.ftc.teamcode.robot.RobotMap;
import org.firstinspires.ftc.teamcode.robot.subsystems.Intake;


@Autonomous(name="TestAuto", group="Auto")
//@Disabled
public class AutoBot extends LinearOpMode {

    private final ElapsedTime runtime = new ElapsedTime();
    private Intake intake;
    private Shooter shooter;

    int MAX_SHOOTER = -2400;

    @Override
    public void runOpMode() {
        RobotMap.robotInit(hardwareMap);
        SampleMecanumDrive drive = new SampleMecanumDrive(hardwareMap);

        intake = new Intake();
        shooter = new Shooter();
        Shooter.setIsShooting(false);
        Intake.setIsIntakeTriggered(false);

        drive.setPoseEstimate(new Pose2d(0, 0, 0));

        Trajectory toSideForGoals = drive.trajectoryBuilder(new Pose2d())
                .splineToSplineHeading(new Pose2d(30, 30, Math.PI), Math.toRadians(0))
                .splineToSplineHeading(new Pose2d(60, 30, Math.PI), 0)
                .addDisplacementMarker(60, () -> {
                    //shooter.setShooterVelocity(MAX_SHOOTER, true);
                })
                .build();

        Pose2d pose2dTemp = toSideForGoals.end();

        waitForStart();
        //drive.followTrajectory(toSideForGoals);
        sleep(1000);

        for (int i = 1; i < 3; i++) {
            shootGoals();
            Trajectory strafeForGoals = drive.trajectoryBuilder(pose2dTemp)
                    .strafeLeft(7.5)
                    .build();
            //drive.followTrajectory(strafeForGoals);
            sleep(1000);
            pose2dTemp = strafeForGoals.end().plus(new Pose2d(0, 0,0));
        }

    }

    public void shootGoals () {
            shooter.setTriggerRollerOpenLoop(0.7);
            sleep(100);
            shooter.setTriggerRollerOpenLoop(0.3);
            sleep(100);
            shooter.setTriggerRollerOpenLoop(0.5);
    }
}
