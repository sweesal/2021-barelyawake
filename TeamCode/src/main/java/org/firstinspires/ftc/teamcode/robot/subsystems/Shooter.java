package org.firstinspires.ftc.teamcode.robot.subsystems;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.robot.RobotMap;

public class Shooter {

    private static final double MAX_SPEED = 2100;
    private final DcMotorEx shooter = RobotMap.shooter;
    private final Servo trigger = RobotMap.trigger;

    public static void setIsShooting(boolean isShooting) {
        Shooter.isShooting = isShooting;
    }

    public static boolean isIsShooting() {
        return isShooting;
    }

    private static boolean isShooting = false; //default state;
    private double triggerNum = 0;
    private double btnNum = 0;

    private double ringLoad = 0;
    private boolean isEmpty = true;
    private ElapsedTime countTimer = new ElapsedTime();

    private boolean isShoot = false;
    private static boolean isTriggered = false;


    public Shooter() {
        shooter.setDirection(DcMotorSimple.Direction.FORWARD);
        shooter.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);
        shooter.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        shooter.setVelocityPIDFCoefficients(62, 2.5 ,15, 0);
        trigger.setDirection(Servo.Direction.FORWARD);
    }


    private void setShooterPower(double power) {
        shooter.setPower(power);
    }

    private void setShooterVelocity(double power) {
        shooter.setPower(power);
    }

    public void setShooter (boolean isBtnPressed) {
        int velocity = 0;
        if (isBtnPressed)
            isShooting = !isShooting;
        if (isShooting) {
            velocity = (int)MAX_SPEED;
            //setShooter(0.875);//shooting power
        } else {
            velocity = 0;
            //setShooter(0);//stop shooting
        }
        setShooterVelocity(velocity);
    }

    public void setShooterVelocityForceRun (int velocity) {
        if (isIsShooting()) {
            shooter.setVelocity(velocity);
        } else {
            shooter.setVelocity(0);
        }
    }


    public void setTriggerFree (double inputPos) {
        double pos = Range.clip(inputPos, -0.95, 0.95);
        trigger.setPosition(pos);
    }

    public void setTrigger (boolean isBtnPressed) {
        if (isBtnPressed)
            setTriggerFree(0.525);
        else
            setTriggerFree(0.625);
    }

    public double getShooterVelocity() {
        return shooter.getVelocity();
    }
}
