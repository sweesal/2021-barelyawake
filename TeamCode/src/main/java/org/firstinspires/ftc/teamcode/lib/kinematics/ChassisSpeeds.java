package org.firstinspires.ftc.teamcode.lib.kinematics;

import org.firstinspires.ftc.teamcode.lib.geometry.Rotation2d;

public class ChassisSpeeds {

    public double vx;
    public double vy;
    public double omega;

    public ChassisSpeeds() {}

    public ChassisSpeeds(double vY, double vX, double omega) {
        this.vy = vY;
        this.vx = vX;
        this.omega = omega;
    }

    public static ChassisSpeeds fromFieldRelativeSpeeds(
            double vyMetersPerSecond,
            double vxMetersPerSecond,
            double omegaRadiansPerSecond,
            Rotation2d robotAngle) {
        return new ChassisSpeeds(
                vyMetersPerSecond * robotAngle.cos() - vxMetersPerSecond * robotAngle.sin(),
                vyMetersPerSecond * robotAngle.sin() + vxMetersPerSecond * robotAngle.cos(),
                omegaRadiansPerSecond);
    }

    public static ChassisSpeeds fromFieldRelativeSpeeds(
            ChassisSpeeds chassisSpeeds, Rotation2d robotAngle) {
        return new ChassisSpeeds(
                chassisSpeeds.vy * robotAngle.cos() - chassisSpeeds.vx * robotAngle.sin(),
                chassisSpeeds.vy * robotAngle.sin() + chassisSpeeds.vx * robotAngle.cos(),
                chassisSpeeds.omega);
    }

    @Override
    public String toString() {
        return String.format(
                "Vx: %.2f cm/s, Vy: %.2f cm/s, Omega: %.2f rad/s",
                vx, vy, omega);
    }

}
