package org.firstinspires.ftc.teamcode.lib.debugPacket;

public class PathConfig {

    public double maxVel = 90;
    public double maxAcc = 25;
    public double maxAngVel = 1;
    public double spacing = 10;
    public double lookAheadDistance = 20;
    public double targetTolerance = 5;
    public double kS = 0;
    public double kV = 0;
    public double kA = 0;

    public PathConfig () {

    }


}
