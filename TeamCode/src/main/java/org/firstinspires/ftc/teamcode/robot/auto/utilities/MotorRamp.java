package org.firstinspires.ftc.teamcode.robot.auto.utilities;

public class MotorRamp {

    private final double maxChangePerMillis;
    private double lastValue;
    private double lastTime;

    public MotorRamp(double maxChangePerSecond) {
        lastValue = 0;
        this.maxChangePerMillis = maxChangePerSecond / 1000.;
    }

    public double applyAsDouble(double value, double timeStamp) {
        if (value > lastValue) {



            lastValue = Math.min(value, lastValue + (timeStamp - lastTime) * maxChangePerMillis);
        } else {
            lastValue = value;
            //lastValue = Math.max(value, lastValue - (timeStamp - lastTime) * maxChangePerMillis);
        }
        lastTime = timeStamp;
        if (Math.abs(value) < 0.01) lastValue = 0;
        return lastValue;
    }

}
