package org.firstinspires.ftc.teamcode.robot.auto.utilities;

import org.firstinspires.ftc.teamcode.robot.auto.config.PIDConfig;

public class ControlMethod {

    //private final ElapsedTime pidTimer = new ElapsedTime();

    public ControlMethod() {

    }

    public PIDConfig applyPIDControl (double timeStamp, PIDConfig pid, double target, double reading) {
        final double dt = pid.prevTimeStamp >= 0 ? timeStamp - pid.prevTimeStamp : 0.0;
        pid.error = target - reading;
        pid.errorRate = (pid.error - pid.prevError) / dt;
        if (pid.error < pid.getIntegralZone()) pid.isIntegralUsed = !pid.isIntegralUsed;
        if (pid.isIntegralUsed) pid.errorSum += pid.error * dt;
        pid.output = pid.error * pid.getKP()
                + (pid.isIntegralUsed ? pid.getKI() * pid.errorSum : 0)
                + pid.getKD() * pid.errorRate;
        pid.prevError = pid.error;
        pid.prevTimeStamp = timeStamp;
        return pid;
    }

}
