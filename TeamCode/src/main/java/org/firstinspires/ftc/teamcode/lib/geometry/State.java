package org.firstinspires.ftc.teamcode.lib.geometry;

public interface State<S>  {
    double distance(final S other);

    boolean equals(final Object other);

    String toString();

    String toCSV();
}
