package org.firstinspires.ftc.teamcode.robot.subsystems;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.robot.RobotMap;

public class Intake {

    private final DcMotor intake = RobotMap.intake;

    public static void setIsIntakeTriggered(boolean isIntakeTriggered) {
        Intake.isIntakeTriggered = isIntakeTriggered;
    }

    public static boolean isIsIntakeTriggered() {
        return isIntakeTriggered;
    }

    private static boolean isIntakeTriggered = false;


    private static final double intakePower = 0.9;

    public Intake() {
        intake.setDirection(DcMotorSimple.Direction.REVERSE);
    }

    public void setIntakeFree (boolean isBtnPressed, double setPower) {
        if (isBtnPressed) {
            intake.setPower(Range.clip(setPower, -0.99, 0.99));
        } else {
            intake.setPower(0);
        }
    }

    public void setIntakeWithShooter (boolean isTriggered) {
        double intakePower = 0;
        if (isTriggered)
            isIntakeTriggered = !isIntakeTriggered;
    }

    public void updateIntakeSingle(boolean isBtnPressed, boolean isRevBtnPressed) {
        double intakePower = 0;
        if (isBtnPressed)
            isIntakeTriggered = !isIntakeTriggered;
        if (isIntakeTriggered)
            intakePower = 0.9;
        if (isRevBtnPressed) {
            intakePower = -0.9;
            isIntakeTriggered = false;
        }
        intake.setPower(intakePower);//stop intake
    }

    public void updateIntakeDual(boolean isBtnPressed, boolean isRevBtnPressed) {
        double intakePower = 0;

        if (isBtnPressed)
            intakePower = 0.9;
        if (isRevBtnPressed) {
            intakePower = -0.9;
        }
        intake.setPower(intakePower);//stop intake
    }


}
